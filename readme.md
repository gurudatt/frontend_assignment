Front End Engineering Assignment
======================

### Read this first

You are required to consume the API listed below to create an interface.  

The interface must demonstrate the following:  
1. Picking the right chart based on the data.  
2. Filtering information in the chart based on each parameter available in the API.  
3. Making the latest information available (The data exposed by the API updates regularly).  

We will be looking for the following things in your submission:  
1. Clarity and legibility of the interface.  
2. Structure of code.  
3. Structuring of information and  choice of charts.   

You may choose to represent all information in a single chart per API endpoint or use multiple charts.  
The interface must be demonstrable.  
Document your code and your approach to the solution in as much detail as possible.  


You may choose to use Javascript, Python or a combination of the two.  
Share the code and documentation as a private gitlab or github repo. Merge requests **will be ignored**.  
You have 3 working days to complete the assignment.  

The details for the API are listed below:  

Using the API
==============
The API is available here: `http://13.84.149.217:8000`  
The API is rate limited and you will be working concurrently on this assignment with others.  

The API has two end points, which are used for the charts.  

RETURN ORDER COUNT  
---------------
GET `/returns/count/order_date{?groupby,category,start_date,end_date}`

Example URI

GET `/returns/count/order_date?start_date=1995-01-01&end_date=2026-01-01`

URI Parameters

   groupby - string (optional) Default: day        
    
Choices: day month week

category -string (optional) Default: All categories (Detailed list at the end)  


start_date - string (required) 
The starting date for the orders

end_date - string (required) 
The ending date for the orders

RETURN REASONS COUNT  
-----------------------

GET `/returns/countdistinct/return_reason{?category,start_date,end_date,limit}`

Shows the distribution of the various reasons for the return.

Example URI

GET `returns/countdistinct/return_reason?start_date=1995-01-01&end_date=2026-01-01`

limit -
snteger (optional) Default: 10 
The amount of data elements to return.

category -
string (optional) Default: All categories 

start_date -
string (required) 
The starting date for the orders

end_date -
string (required) 
The ending date for the orders


##### Available Categories

 Men's Footwear    
 Fitness   
 Mobiles and Tablets    
 Mobile Accessories    
 Women's Watches    
 Tools   
 Men's Watches    
 Women's Clothing    
 Kitchen Appliances   
 Women's Footwear   
 Eyewear   
 Computer Accessories   
 Men's Clothing    
 Men's Grooming   

